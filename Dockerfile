FROM ubuntu

ENV TZ=Europe/Paris
ENV DEBIAN_FRONTEND=noninteractive
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update \
&& apt-get install -y vlc

RUN useradd -m vlcuser

COPY ssi-video.sh /tmp/
ENTRYPOINT ["/tmp/ssi-video.sh"]
